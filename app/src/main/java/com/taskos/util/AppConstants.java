package com.taskos.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public final class AppConstants {

    public final static String ownerId = "ec087c0f-7938-406c-bea1-1b10dd8abf4b";

    //Customers
    public static String[] kStatusArray = {"Open Lead", "Appointment Set", "Quoted", "Contacted", "Not Contacted", "Pre-Qualified", "Not Qualified", "Sold", "Job Completed", "Recurring Customer", "Highly Valued Customer", "Do Not Contact", "Not Qualified", "Junk Lead", "Closed Did Not Win", "Contact in Future"};
    public static String[] kOpenLeadsPulledArray = {"Open Lead", "Appointment Set", "Quoted", "Contacted", "Not Contacted", "Pre-Qualified", "Not Qualified"};
    public static String[] kClosedCustomerArray = {"Sold", "Job Completed", "Recurring Customer", "Highly Valued Customer"};
    //Tasks
    public static String[] kCompletionStatusArray = {"Open", "Completed"};
    public static String[] kUnsoldCustomers = {"Not Qualified", "Closed Did Not Win", "Junk Lead", "Contact in Future", "Do Not Contact"};
    //Permissions
    public static String[] kCustomerPermissionsArray = {"Access Customers", "See All Leads", "Edit Customer", "Edit Salesperson", "Delete Customer", "Change Customer Status", "Delete Customer Comment"};
    public static String[] kJobPermissionsArray = {"Access Jobs", "Add Job", "Edit Job", "Change Job Status", "Assign Employees to Job", "See Financials", "Delete Job Comment", "Share Job"};
    public static String[] kTaskPermissionArray = {"See All Tasks", "Assign Task to Employee", "Delete Task"};
    public static String[] kCompanyPermissionsArray = {"Add Employees", "Remove Employees", "Grant Permissions", "Edit Company"};

    public static List<String> getkStatusArray() {
        List<String> kStatusArray2 = new ArrayList<>();
        kStatusArray2.add("Appointment Set");
        kStatusArray2.add("Quoted");
        kStatusArray2.add("Contacted");
        kStatusArray2.add("Not Contacted");
        kStatusArray2.add("Pre-Qualified");
        kStatusArray2.add("Not Qualified");
        kStatusArray2.add("Sold");
        kStatusArray2.add("Job Completed");
        kStatusArray2.add("Recurring Customer");
        kStatusArray2.add("Highly Valued Customer");
        kStatusArray2.add("Do Not Contact");
        kStatusArray2.add("Not Qualified");
        kStatusArray2.add("Junk Lead");
        kStatusArray2.add("Closed Did Not Win");
        kStatusArray2.add("Contact in Future");
        return kStatusArray2;
    }

    private AppConstants() {
        // This class is not publicly instantiable
    }
}
