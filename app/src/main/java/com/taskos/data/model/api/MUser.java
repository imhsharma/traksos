package com.taskos.data.model.api;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class MUser {

    private String uuid;
    private String userFirstName;
    private String userLastName;
    private String email;
    private String phone;
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String company;
    private String companyUuid;
    private String[] permissionLevel;
    private String jobRole;
    private String username;
    private String userProfileImageLink;
    private String userSubscriptionStart;
    private String userSubscriptionEnd;
    private boolean userPaid;
    private boolean userApproveCompanyAdd;
    private boolean userCompanyAddRequested;
    private boolean onBoarded;
    private String[] token;
    private String[] userNetwork;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyUuid() {
        return companyUuid;
    }

    public void setCompanyUuid(String companyUuid) {
        this.companyUuid = companyUuid;
    }

    public String[] getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(String[] permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public String getJobRole() {
        return jobRole;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserProfileImageLink() {
        return userProfileImageLink;
    }

    public void setUserProfileImageLink(String userProfileImageLink) {
        this.userProfileImageLink = userProfileImageLink;
    }

    public String getUserSubscriptionStart() {
        return userSubscriptionStart;
    }

    public void setUserSubscriptionStart(String userSubscriptionStart) {
        this.userSubscriptionStart = userSubscriptionStart;
    }

    public String getUserSubscriptionEnd() {
        return userSubscriptionEnd;
    }

    public void setUserSubscriptionEnd(String userSubscriptionEnd) {
        this.userSubscriptionEnd = userSubscriptionEnd;
    }

    public boolean isUserPaid() {
        return userPaid;
    }

    public void setUserPaid(boolean userPaid) {
        this.userPaid = userPaid;
    }

    public boolean isUserApproveCompanyAdd() {
        return userApproveCompanyAdd;
    }

    public void setUserApproveCompanyAdd(boolean userApproveCompanyAdd) {
        this.userApproveCompanyAdd = userApproveCompanyAdd;
    }

    public boolean isUserCompanyAddRequested() {
        return userCompanyAddRequested;
    }

    public void setUserCompanyAddRequested(boolean userCompanyAddRequested) {
        this.userCompanyAddRequested = userCompanyAddRequested;
    }

    public boolean isOnBoarded() {
        return onBoarded;
    }

    public void setOnBoarded(boolean onBoarded) {
        this.onBoarded = onBoarded;
    }

    public String[] getToken() {
        return token;
    }

    public void setToken(String[] token) {
        this.token = token;
    }

    public String[] getUserNetwork() {
        return userNetwork;
    }

    public void setUserNetwork(String[] userNetwork) {
        this.userNetwork = userNetwork;
    }
}
