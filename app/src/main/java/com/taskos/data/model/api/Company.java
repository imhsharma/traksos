package com.taskos.data.model.api;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class Company {

    private String name;
    private String uuid;
    private String dateAdded;
    private int userCount;
    private String[] employeeList;
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String phone;
    private String email;
    private String subscriptionEnd;
    private String[] administrator;
    private String[] requestedEmployees;
    private String website;
    private boolean hasWebsite;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public String[] getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(String[] employeeList) {
        this.employeeList = employeeList;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubscriptionEnd() {
        return subscriptionEnd;
    }

    public void setSubscriptionEnd(String subscriptionEnd) {
        this.subscriptionEnd = subscriptionEnd;
    }

    public String[] getAdministrator() {
        return administrator;
    }

    public void setAdministrator(String[] administrator) {
        this.administrator = administrator;
    }

    public String[] getRequestedEmployees() {
        return requestedEmployees;
    }

    public void setRequestedEmployees(String[] requestedEmployees) {
        this.requestedEmployees = requestedEmployees;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isHasWebsite() {
        return hasWebsite;
    }

    public void setHasWebsite(boolean hasWebsite) {
        this.hasWebsite = hasWebsite;
    }
}
