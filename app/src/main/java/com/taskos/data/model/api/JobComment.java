package com.taskos.data.model.api;

import java.util.Date;

/**
 * Created by Hemant Sharma on 28-02-20.
 * Divergent software labs pvt. ltd
 */
public class JobComment {

    private String uuid;
    private String userFirstName;
    private String userLastName;
    private String userUuid;
    private String username;
    private String jobUuid;
    private String comment;
    private Date commentPostedTime;
    private String customerStatus; //(pass empty string)
    private String imageLink;
    private String owner;

}
