package com.taskos.data.local.prefs;


import android.app.Activity;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public interface PreferencesHelper {

    boolean isLoggedIn();

    void setLoggedIn(boolean isLoggedIn);

    void logout(Activity activity);
}
