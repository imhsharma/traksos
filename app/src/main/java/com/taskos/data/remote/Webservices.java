package com.taskos.data.remote;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public final class Webservices {

    static final String BASE_URL = "https://";

    private Webservices() {
        // This class is not publicly instantiable
    }
}
