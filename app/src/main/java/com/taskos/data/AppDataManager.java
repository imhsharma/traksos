package com.taskos.data;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taskos.data.local.prefs.AppPreferencesHelper;
import com.taskos.data.local.prefs.PreferencesHelper;
import com.taskos.data.remote.ApiHelper;
import com.taskos.data.remote.AppApiHelper;


/**
 * Created by Hemant Sharma on 23-02-20.
 */


public final class AppDataManager implements DataManager {

    private static AppDataManager instance;
    private final Gson mGson;
    private final ApiHelper mApiHelper;
    private final PreferencesHelper mPreferencesHelper;


    private AppDataManager(Context context) {
        mPreferencesHelper = new AppPreferencesHelper(context);
        mApiHelper = AppApiHelper.getAppApiInstance();
        mGson = new GsonBuilder().create();
    }

    public synchronized static AppDataManager getInstance(Context context) {
        if (instance == null) {
            instance = new AppDataManager(context);
        }
        return instance;
    }

    @Override
    public boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public void setLoggedIn(boolean isLoggedIn) {
        mPreferencesHelper.setLoggedIn(isLoggedIn);
    }

    @Override
    public void logout(Activity activity) {
        mPreferencesHelper.logout(activity);
    }
}
