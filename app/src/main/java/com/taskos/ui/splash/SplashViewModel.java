package com.taskos.ui.splash;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class SplashViewModel extends BaseViewModel<SplashNavigator> {

    public SplashViewModel(DataManager dataManager) {
        super(dataManager);
    }

    public void decideNextActivity() {
        getNavigator().openLoginActivity();
    }
}
