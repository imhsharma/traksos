

package com.taskos.ui.splash;

/**
 * Created by Hemant Sharma on 23-02-20.
 */

public interface SplashNavigator {

    void openLoginActivity();

    void openMainActivity();
}
