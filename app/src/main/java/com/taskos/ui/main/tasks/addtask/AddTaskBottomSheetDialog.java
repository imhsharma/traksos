package com.taskos.ui.main.tasks.addtask;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.ToDo;
import com.taskos.databinding.DialogAddTaskBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.util.AppConstants;
import com.taskos.util.CalenderUtils;

import java.util.Date;
import java.util.UUID;

import static com.taskos.util.CommonUtils.getCurrentDateTime;


public class AddTaskBottomSheetDialog extends BaseBottomSheetDialog<DialogAddTaskBinding, AddTaskViewModel> implements View.OnClickListener, AddTaskNavigator {

    private static final String TAG = "AddClientBottomSheetDia";
    private AddTaskViewModel addTaskViewModel;
    private Date appointmentTime;
    private int mYear, mMonth, mDay, mHour, mMinute;

    public static AddTaskBottomSheetDialog newInstance() {

        Bundle args = new Bundle();

        AddTaskBottomSheetDialog fragment = new AddTaskBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_task;
    }

    @Override
    public AddTaskViewModel getViewModel() {
        addTaskViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddTaskViewModel.class);
        return addTaskViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvSaveTask.setOnClickListener(this);
        getViewDataBinding().rlTaskCompletion.setOnClickListener(this);

        getViewDataBinding().switchCoworker.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getViewDataBinding().rlAssignUserName.setVisibility(View.VISIBLE);
                getViewDataBinding().viewAssignName.setVisibility(View.VISIBLE);
            } else {
                getViewDataBinding().rlAssignUserName.setVisibility(View.GONE);
                getViewDataBinding().viewAssignName.setVisibility(View.GONE);
            }
        });
        getViewDataBinding().switch1.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                getViewDataBinding().rlJob.setVisibility(View.VISIBLE);
                getViewDataBinding().viewLineJob.setVisibility(View.VISIBLE);
            } else {
                getViewDataBinding().rlJob.setVisibility(View.GONE);
                getViewDataBinding().viewLineJob.setVisibility(View.GONE);
            }
        });
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSaveTask:
                getViewDataBinding().edtTaskToComplete.getText().toString();
                getViewDataBinding().edtTaskNote.getText().toString();
                getViewDataBinding().edtDateTargetCompletion.getText().toString();

                if (isNetworkConnected()) {
                    if (verifyInputs()) {
                        ToDo toDo = new ToDo();
                        toDo.setCustomerUuid(AppConstants.ownerId);
                        toDo.setUuid(UUID.randomUUID().toString());
                        toDo.setTask(getViewDataBinding().edtTaskToComplete.getText().toString());
                        toDo.setCompletionStatus(getViewDataBinding().edtTaskToComplete.getText().toString());
                        toDo.setDateEntered(getCurrentDateTime());
                        toDo.setDateCompleted(new Date(getViewDataBinding().edtDateTargetCompletion.getText().toString()));
                        toDo.setCompletedBy("");
                        toDo.setCompletedByUuid("");
                        toDo.setAssignedBy(getViewDataBinding().tvAssignUserName.getText().toString());
                        toDo.setOwner("Test Uesr");
                        toDo.setJobUuid("12");
                        toDo.setJobName(getViewDataBinding().tvJob.getText().toString());
                        toDo.setNotes(getViewDataBinding().edtTaskNote.getText().toString());
                        addTaskViewModel.doAddTask(toDo);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }


                break;
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;
            case R.id.rlTaskCompletion:
                getViewDataBinding().view.setVisibility(View.VISIBLE);
                getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    appointmentTime = date;
                    getViewDataBinding().edtDateTargetCompletion.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;
        }
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().edtTaskToComplete.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_complete_task));
            return false;
        } else return true;
    }


    @Override
    public void openDashboard() {
        dismissDialog(TAG);
    }
}
