package com.taskos.ui.main.openjobs.schedule;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

public class JobScheduleViewModel extends BaseViewModel<BaseNavigator> {
    public JobScheduleViewModel(DataManager dataManager) {
        super(dataManager);
    }
}