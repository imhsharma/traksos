package com.taskos.ui.main.openjobs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

public class OpenJobsViewModel extends BaseViewModel<OpenJobsNavigator> {

    private MutableLiveData<String> mText;


    public OpenJobsViewModel(DataManager dataManager) {
        super(dataManager);
        mText = new MutableLiveData<>();
        mText.setValue("Open Jobs View is Under Development");
    }

    public LiveData<String> getText() {
        return mText;
    }
}