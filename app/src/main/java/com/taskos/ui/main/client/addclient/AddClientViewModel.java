package com.taskos.ui.main.client.addclient;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewModel;

public class AddClientViewModel extends BaseViewModel<AddClientNavigator> {

    private FirebaseFirestore mFirestore;

    public AddClientViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }


    public void doAddClient(Customer customer) {
        // Get a reference to the Customers collection
        CollectionReference customerRef = mFirestore.collection("Customers");

        customerRef.add(customer).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().openDashboard();
            }
        });
    }
}