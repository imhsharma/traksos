package com.taskos.ui.main.tasks.detail.mytaskdetail;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.ToDo;
import com.taskos.databinding.FragmentTaskDetailsBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.util.CalenderUtils;

import java.util.Date;


public class TaskDetailsFragment extends BaseFragment<FragmentTaskDetailsBinding, TasksDetailViewModel>
        implements TasksDetailNavigator, View.OnClickListener {

    private TasksDetailViewModel tasksDetailViewModel;
    private String taskKey = "";
    private ToDo toDo;
    private Date taskCompletionDate, taskEnteredDate;


    public static TaskDetailsFragment newInstance(String taskKey) {
        Bundle args = new Bundle();
        TaskDetailsFragment fragment = new TaskDetailsFragment();
        args.putString("TaskKey", taskKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.img_back).setOnClickListener(this);
        view.findViewById(R.id.rlMarkComplete).setOnClickListener(this);

        tasksDetailViewModel.getTaskDetail(taskKey).observe(getViewLifecycleOwner(), toDo -> {
            this.toDo = toDo;
            getViewDataBinding().tvTaskNote.setText(toDo.getNotes());
            getViewDataBinding().tvTaskOwner.setText(toDo.getOwner());
            getViewDataBinding().tvTaskName.setText(toDo.getTask());
            getViewDataBinding().etAssignedUser.setText(toDo.getAssignedBy());
            taskCompletionDate = toDo.getDateCompleted();
            taskEnteredDate = toDo.getDateEntered();
            getViewDataBinding().etTargetCompletion.setText(CalenderUtils.formatDate(toDo.getDateCompleted(),
                    CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(toDo.getDateCompleted(), CalenderUtils.TIME_FORMAT_AM)));
            getViewDataBinding().etTaskentered.setText(CalenderUtils.formatDate(toDo.getDateEntered(),
                    CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(toDo.getDateEntered(), CalenderUtils.TIME_FORMAT_AM)));
        });
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_task_details;
    }

    @Override
    public TasksDetailViewModel getViewModel() {
        tasksDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(TasksDetailViewModel.class);
        return tasksDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tasksDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            taskKey = getArguments().getString("TaskKey");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;
            case R.id.rlMarkComplete:
                getViewDataBinding().tvMarkComplete.setText(getResources().getString(R.string.markOpen));
                break;
        }
    }
}