package com.taskos.ui.main.more;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

public class MoreViewModel extends BaseViewModel<MoreNavigator> {

    private MutableLiveData<String> mText;

    public MoreViewModel(DataManager dataManager) {
        super(dataManager);
        mText = new MutableLiveData<>();
        mText.setValue("More View is Under Development");
    }

    public LiveData<String> getText() {
        return mText;
    }
}