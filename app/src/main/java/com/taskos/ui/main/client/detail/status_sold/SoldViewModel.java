package com.taskos.ui.main.client.detail.status_sold;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.JobFinancial;
import com.taskos.ui.base.BaseViewModel;

public class SoldViewModel extends BaseViewModel<SoldNavigator> {

    private static final String TAG = "SoldViewModel";
    private FirebaseFirestore mFirestore;
    private MutableLiveData<Customer> mutableLiveData = new MutableLiveData<>();

    public SoldViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public MutableLiveData<Customer> getClientDetail(String customerKey) {
        DocumentReference query = mFirestore.collection("Customers").document(customerKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Customer customer = documentSnapshot.toObject(Customer.class);
                mutableLiveData.setValue(customer);
            }
        });

        return mutableLiveData;
    }

    public void doSaveSaleInformation(String customerKey, Customer customer, JobFinancial jobFinancial) {
        // Get a reference to the Customers collection
        CollectionReference jobFinancialRef = mFirestore.collection("JobFinancial");
        jobFinancialRef.add(jobFinancial).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                mFirestore.collection("Customers").document(customerKey)
                        .set(customer).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        getNavigator().dismissDialog();
                    }
                });
            }
        });
    }
}