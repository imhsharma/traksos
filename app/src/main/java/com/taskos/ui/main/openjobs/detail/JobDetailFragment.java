package com.taskos.ui.main.openjobs.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentJobDetailBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.openjobs.schedule.JobScheduleBottomSheetDialog;
import com.taskos.util.CalenderUtils;

import java.util.Date;

public class JobDetailFragment extends BaseFragment<FragmentJobDetailBinding, JobDetailViewModel> implements JobDetailNavigator, View.OnClickListener {

    private JobDetailViewModel jobDetailViewModel;
    private String customerKey = "";
    private String lastDate = "";
    private Date soldTime;

    public static JobDetailFragment newInstance(String customerKey) {

        Bundle args = new Bundle();

        JobDetailFragment fragment = new JobDetailFragment();
        args.putString("CustomerKey", customerKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_job_detail;
    }

    @Override
    public JobDetailViewModel getViewModel() {
        jobDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(JobDetailViewModel.class);
        return jobDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jobDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            customerKey = getArguments().getString("CustomerKey");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.open_jobs);
        tvTitle.setOnClickListener(this);
        view.findViewById(R.id.img_back).setOnClickListener(this);
        getViewDataBinding().rlShare.setOnClickListener(this);
        getViewDataBinding().rlSubscribe.setOnClickListener(this);
        getViewDataBinding().rlJobFinancial.setOnClickListener(this);
        getViewDataBinding().rlCustDetail.setOnClickListener(this);
        getViewDataBinding().rlShowImages.setOnClickListener(this);
        getViewDataBinding().rlShowDocument.setOnClickListener(this);
        getViewDataBinding().tvDirection.setOnClickListener(this);
        getViewDataBinding().tvJobSchedule.setOnClickListener(this);
        getViewDataBinding().tvEditJob.setOnClickListener(this);
        getViewDataBinding().tvSaveJobInfo.setOnClickListener(this);
        getViewDataBinding().tvSoldTime.setOnClickListener(this);

       /* jobDetailViewModel.getJobDetail(customerKey).observe(getViewLifecycleOwner(), customer -> {
       lastDate = "";
            getViewDataBinding().etSalesPersonName.setText(customer.getSalesperson());
            getViewDataBinding().tvNameMap.setText(customer.getName());
            getViewDataBinding().tvName.setText(customer.getName());
            getViewDataBinding().etName.setText(customer.getName());
            getViewDataBinding().etProductInterest.setText(customer.getProductOfInterest());
            appointmentTime = customer.getAppointmentTime();
            getViewDataBinding().tvStreet.setText(customer.getStreetAddress());
            getViewDataBinding().tvAddress.setText(customer.getCity().concat(", ").concat(customer.getState()).concat(" ").concat(customer.getZipcode()));
        });*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.rl_share:
                break;

            case R.id.rl_subscribe:
                break;

            case R.id.rl_job_financial:
                break;

            case R.id.rl_cust_detail:
                break;

            case R.id.rl_show_images:
                break;

            case R.id.rl_show_document:
                break;

            case R.id.tv_direction:
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:22.7196, 75.8577?q=22.7196, 75.8577(Indore)"));
                /*Uri gmmIntentUri = Uri.parse("geo: 22.7196, 75.8577");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);*/
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getBaseActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                break;

            case R.id.tv_edit_job:
                hideKeyboard();
                if (getViewDataBinding().tvEditJob.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditJob.setText(R.string.cancel);
                    enableDisableJobInformation(true);
                } else {
                    getViewDataBinding().tvSoldTime.setText(lastDate);
                    getViewDataBinding().tvEditJob.setText(R.string.edit);
                    enableDisableJobInformation(false);
                }
                break;

            case R.id.tv_sold_time:
                if (getViewDataBinding().tvEditJob.getText().toString().equals("Cancel")) {
                    getViewDataBinding().llSalesPerson.setVisibility(View.GONE);
                    getViewDataBinding().viewSalesPerson.setVisibility(View.GONE);
                    getViewDataBinding().viewTime.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateSoldTimePicker.setVisibility(View.VISIBLE);
                    getViewDataBinding().dateSoldTimePicker.addOnDateChangedListener((displayed, date) -> {
                        soldTime = date;
                        getViewDataBinding().tvSoldTime.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                    });
                }
                break;

            case R.id.tv_save_job_info:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        enableDisableJobInformation(false);
                        //save job info
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_job_schedule:
                JobScheduleBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;
        }
    }

    private boolean verifyInput() {
        if (getViewDataBinding().etJobName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_job_name_empty));
            return false;
        } else return true;
    }

    private void enableDisableJobInformation(boolean isEnable) {
        if (!isEnable) {
            getViewDataBinding().viewTime.setVisibility(View.GONE);
            getViewDataBinding().dateSoldTimePicker.setVisibility(View.GONE);
        }
        getViewDataBinding().rlJobFinancial.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().llSalesPerson.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().viewSalesPerson.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().tvSaveJobInfo.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().etJobName.setEnabled(isEnable);
        getViewDataBinding().etSoldItem.setEnabled(isEnable);
    }

}