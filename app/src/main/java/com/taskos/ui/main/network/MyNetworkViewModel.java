package com.taskos.ui.main.network;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseViewModel;

public class MyNetworkViewModel extends BaseViewModel<MyNetworkNavigator> {

    private MutableLiveData<String> mText;


    public MyNetworkViewModel(DataManager dataManager) {
        super(dataManager);
        mText = new MutableLiveData<>();
        mText.setValue("MyNetwork View is Under Development");
    }

    public LiveData<String> getText() {
        return mText;
    }
}