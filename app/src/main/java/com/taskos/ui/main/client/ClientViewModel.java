package com.taskos.ui.main.client;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppConstants;
import com.taskos.util.AppLogger;

import java.util.HashMap;

public class ClientViewModel extends BaseViewModel<ClientNavigator> {

    private static final String TAG = "ClientViewModel";
    private FirebaseFirestore mFirestore;
    private HashMap<String, Customer> customerHashMap = new HashMap<>();
    private MutableLiveData<HashMap<String, Customer>> mutableLiveData = new MutableLiveData<>();


    public ClientViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public MutableLiveData<HashMap<String, Customer>> getOpenClients(String value, int currentPos, String search) {
        customerHashMap.clear();
        Query query;
        if (currentPos == 0) {
            if (search.isEmpty()) {
                query = mFirestore.collection("Customers")
                        .whereEqualTo("customerStatus", value)
                        .orderBy("appointmentEntered", Query.Direction.DESCENDING);
            } else {
                query = mFirestore.collection("Customers")
                        .whereEqualTo("name", search)
                        .orderBy("appointmentEntered", Query.Direction.DESCENDING);
            }
        } else if (currentPos == 1) {
            if (search.isEmpty()) {
                query = mFirestore.collection("Customers")
                        .whereEqualTo("owner", AppConstants.ownerId);
            } else {
                query = mFirestore.collection("Customers")
                        .whereEqualTo("name", search)
                        .whereEqualTo("owner", AppConstants.ownerId);
            }
        } else {
            if (search.isEmpty()) {
                query = mFirestore.collection("Customers")
                        .whereEqualTo("customerStatus", value)
                        .orderBy("appointmentEntered", Query.Direction.DESCENDING);
            } else {
                query = mFirestore.collection("Customers")
                        .whereEqualTo("name", search)
                        .whereEqualTo("customerStatus", value)
                        .orderBy("appointmentEntered", Query.Direction.DESCENDING);
            }
        }

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                customerHashMap.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    AppLogger.e("Test", "" + doc.getId());
                    Customer customer = doc.toObject(Customer.class);
                    customerHashMap.put(doc.getId(), customer);
                    mutableLiveData.setValue(customerHashMap);
                }
            }
        });

        return mutableLiveData;
    }
}