package com.taskos.ui.main.client;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.util.CalenderUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class ClientAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static int VIEW_TYPE_COMMON = 1;
    public static int VIEW_TYPE_CLIENT = 2;
    private ItemClickListener callback;
    private HashMap<String, Customer> customerList;
    private List<String> mKeys;
    private int viewType;

    public ClientAdapter(int viewType, HashMap<String, Customer> customerList, ItemClickListener callback) {
        this.viewType = viewType;
        this.customerList = customerList;
        mKeys = new ArrayList<>(customerList.keySet());
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_CLIENT) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_client_leads, parent, false));
        } else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_open_leads, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_TYPE_CLIENT) {
            return VIEW_TYPE_CLIENT;
        } else {
            return VIEW_TYPE_COMMON;
        }
    }

    @Override
    public int getItemCount() {
        if (customerList != null && customerList.size() > 0) {
            return customerList.size();
        } else {
            return 0;
        }
    }

    public Customer getItem(int position) {
        return customerList.get(mKeys.get(position));
    }

    public interface ItemClickListener {
        void onItemClick(String userKey);
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_salesperson, tv_status, tv_appoint_time, tv_city;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_salesperson = itemView.findViewById(R.id.tv_salesperson);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_appoint_time = itemView.findViewById(R.id.tv_appoint_time);
            tv_city = itemView.findViewById(R.id.tv_city);

            itemView.setOnClickListener(view -> {
                if (callback != null && getAdapterPosition() != -1) {
                    callback.onItemClick(mKeys.get(getAdapterPosition()));
                }
            });
        }

        public void onBind(int position) {
            Customer tmpBean = getItem(position);

            if (viewType == VIEW_TYPE_CLIENT) {
                tv_name.setText(tmpBean.getName());
                tv_salesperson.setText(tmpBean.getSalesperson());
                tv_status.setText(tmpBean.getCustomerStatus());
            } else {
                tv_name.setPaintFlags(tv_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tv_name.setText(tmpBean.getName());
                tv_salesperson.setText(tmpBean.getSalesperson());
                tv_appoint_time.setText(CalenderUtils.formatDate(tmpBean.getAppointmentTime(), CalenderUtils.TIMESTAMP_FORMAT));
                tv_city.setText(tmpBean.getCity());
                tv_status.setText(tmpBean.getCustomerStatus());
            }
        }
    }
}
