package com.taskos.ui.main.tasks.detail.allTaskDetails;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.ToDo;
import com.taskos.databinding.MyTaskDetailViewBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.util.CalenderUtils;

import java.util.Date;

public class AllTaskDetailsFragment extends BaseFragment<MyTaskDetailViewBinding, AllTaskDetailsViewModel>
        implements AllTasksDetailNavigator, View.OnClickListener {

    private AllTaskDetailsViewModel allTasksDetailViewModel;
    private String taskKey = "";
    private Date taskCompletionDate, taskEnteredDate;
    private ToDo toDo;


    public static AllTaskDetailsFragment newInstance(String taskKey) {
        Bundle args = new Bundle();
        AllTaskDetailsFragment fragment = new AllTaskDetailsFragment();
        args.putString("TaskKey", taskKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.img_back).setOnClickListener(this);

        allTasksDetailViewModel.getClientDetail(taskKey).observe(getViewLifecycleOwner(), toDo -> {
            this.toDo = toDo;
            getViewDataBinding().tvAuth.setText(toDo.getOwner());
            getViewDataBinding().etTaksNotes.setText(toDo.getNotes());
            getViewDataBinding().tvName.setText(toDo.getTask());
            taskCompletionDate = toDo.getDateCompleted();
            taskEnteredDate = toDo.getDateEntered();
            getViewDataBinding().tvCompletionDate.setText(CalenderUtils.formatDate(toDo.getDateCompleted(),
                    CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(toDo.getDateCompleted(), CalenderUtils.TIME_FORMAT_AM)));
            getViewDataBinding().etEnteredBy.setText(CalenderUtils.formatDate(toDo.getDateEntered(),
                    CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(toDo.getDateEntered(), CalenderUtils.TIME_FORMAT_AM)));
        });
    }

    @Override
    public int getBindingVariable() { return com.taskos.BR.viewModel; }

    @Override
    public int getLayoutId() {
        return R.layout.my_task_detail_view;
    }

    @Override
    public AllTaskDetailsViewModel getViewModel() {
        allTasksDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AllTaskDetailsViewModel.class);
        return allTasksDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allTasksDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            taskKey = getArguments().getString("TaskKey");
        }
    }


    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.img_back:
               getBaseActivity().onBackPressed();
               break;
       }
    }
}
