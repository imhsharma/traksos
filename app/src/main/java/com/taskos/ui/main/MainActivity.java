package com.taskos.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.ActivityMainBinding;
import com.taskos.ui.base.BaseActivity;
import com.taskos.ui.main.client.ClientFragment;
import com.taskos.ui.main.more.MoreFragment;
import com.taskos.ui.main.network.MyNetworkFragment;
import com.taskos.ui.main.openjobs.OpenJobsFragment;
import com.taskos.ui.main.tasks.TasksFragment;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainNavigator {

    private MainViewModel mainViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mainViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(MainViewModel.class);
        return mainViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel.setNavigator(this);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        replaceFragment(new ClientFragment(), R.id.nav_host_fragment);

        navView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.navigation_client) {
                replaceFragment(new ClientFragment(), R.id.nav_host_fragment);
                return true;
            } else if (item.getItemId() == R.id.navigation_open_jobs) {
                replaceFragment(new OpenJobsFragment(), R.id.nav_host_fragment);
                return true;
            } else if (item.getItemId() == R.id.navigation_tasks) {
                replaceFragment(new TasksFragment(), R.id.nav_host_fragment);
                return true;
            } else if (item.getItemId() == R.id.navigation_network) {
                replaceFragment(new MyNetworkFragment(), R.id.nav_host_fragment);
                return true;
            } else if (item.getItemId() == R.id.navigation_more) {
                replaceFragment(new MoreFragment(), R.id.nav_host_fragment);
                return true;
            }
            return false;
        });
    }


}
