package com.taskos.ui.main.tasks;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentTasksBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;
import com.taskos.ui.main.tasks.detail.allTaskDetails.AllTaskDetailsFragment;
import com.taskos.ui.main.tasks.detail.mytaskdetail.TaskDetailsFragment;
import com.taskos.util.AppConstants;


public class TasksFragment extends BaseFragment<FragmentTasksBinding, TasksViewModel> implements View.OnClickListener {

    private TasksViewModel tasksViewModel;
    private int currentPos = 0;

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_tasks;
    }

    @Override
    public TasksViewModel getViewModel() {
        tasksViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(TasksViewModel.class);
        return tasksViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().fabAddClient.setOnClickListener(this);
        getViewDataBinding().fabAddJob.setOnClickListener(this);
        getViewDataBinding().fabAddTask.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);


        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });
        doAddTextWatcher();
    }


    private void doAddTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    if (editable.toString().length() > 2)
                        filterData(editable.toString());
                } else {
                    filterData("");
                }
            }
        });
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            getViewDataBinding().etSearch.setHint("Search my Tasks");
            doTaskData(AppConstants.kCompletionStatusArray[0], 0, search);
        } else {
            getViewDataBinding().etSearch.setHint("Search All Open Tasks");
            doTaskData(AppConstants.kCompletionStatusArray[1], 1, search);
        }
    }

    private void doTaskData(String value, int currentPos, String search) {
        tasksViewModel.getOpenTask(value, currentPos, search).observe(getViewLifecycleOwner(), customerMap -> {
            LinearLayoutManager llm = new LinearLayoutManager(mActivity);
            TaskAdapter taskAdapter = new TaskAdapter(currentPos == 1 ? TaskAdapter.VIEW_ALL_OPEN_TASK : TaskAdapter.VIEW_TYPE_COMMON, customerMap, new TaskAdapter.ItemClickListener() {
                @Override
                public void onItemClick(String userKey) {
                    if (currentPos == 1){
                        getBaseActivity().addFragment(AllTaskDetailsFragment.newInstance(userKey), R.id.nav_host_fragment, true);
                    }else {
                        getBaseActivity().addFragment(TaskDetailsFragment.newInstance(userKey), R.id.nav_host_fragment, true);
                    }

                }
            });
            getViewDataBinding().rcvTaskList.setLayoutManager(llm);
            getViewDataBinding().rcvTaskList.setAdapter(taskAdapter);
        });
    }

    @Override
    public void onClick(View view) {
        getViewDataBinding().menuFloat.close(true);
        switch (view.getId()) {
            case R.id.fab_add_client:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.fab_add_job:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.fab_add_task:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.tv_search:
                getViewDataBinding().llSearch.setVisibility(getViewDataBinding().llSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }
}