package com.taskos.ui.main.network;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentMyNetworkBinding;
import com.taskos.ui.base.BaseFragment;


public class MyNetworkFragment extends BaseFragment<FragmentMyNetworkBinding, MyNetworkViewModel> {

    private MyNetworkViewModel myNetworkViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my_network;
    }

    @Override
    public MyNetworkViewModel getViewModel() {
        myNetworkViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(MyNetworkViewModel.class);
        return myNetworkViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView textView = view.findViewById(R.id.text_dashboard);
        myNetworkViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
    }
}