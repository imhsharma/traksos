package com.taskos.ui.main.employee;

import androidx.lifecycle.MutableLiveData;

import com.taskos.data.DataManager;
import com.taskos.data.model.other.KeyValue;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class EmployeesViewModel extends BaseViewModel<BaseNavigator> {
    private List<KeyValue> keyValueArrayList = new ArrayList<>();
    private MutableLiveData<List<KeyValue>> mutableLiveData = new MutableLiveData<>();

    public EmployeesViewModel(DataManager dataManager) {
        super(dataManager);
    }

    public MutableLiveData<List<KeyValue>> getEmployees() {

        keyValueArrayList.clear();
        KeyValue keyValue = new KeyValue("", "Abhijeet Ashapure");
        keyValueArrayList.add(keyValue);
        keyValue = new KeyValue("", "Hemant Sharma");
        keyValueArrayList.add(keyValue);
        keyValue = new KeyValue("", "Deependra Singh");
        keyValueArrayList.add(keyValue);
        mutableLiveData.setValue(keyValueArrayList);

        return mutableLiveData;
    }
}