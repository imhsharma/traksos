package com.taskos.ui.main.client.detail.eventrepeat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.other.KeyValue;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class EventRepeatAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ItemClickCallback callback;
    private List<KeyValue> keyValueList;
    private int lastPos = 0;

    public EventRepeatAdapter(List<KeyValue> keyValueList, ItemClickCallback callback) {
        this.keyValueList = keyValueList;
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_repeat, parent, false));
    }

    @Override
    public int getItemCount() {
        if (keyValueList != null && keyValueList.size() > 0) {
            return keyValueList.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name;
        private ImageView img_selected;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            img_selected = itemView.findViewById(R.id.img_selected);

            itemView.setOnClickListener(view -> {
                int tempPos = getAdapterPosition();
                if (tempPos != -1 && callback != null) {
                    keyValueList.get(lastPos).isSelected = false;
                    notifyItemChanged(lastPos);

                    callback.onItemClick(getAdapterPosition());
                    keyValueList.get(tempPos).isSelected = true;
                    notifyItemChanged(tempPos);

                    lastPos = tempPos;
                }
            });
        }

        public void onBind(int position) {
            KeyValue tmpBean = keyValueList.get(position);
            tv_name.setText(tmpBean.getValue());

            img_selected.setVisibility(tmpBean.isSelected ? View.VISIBLE : View.GONE);
        }
    }

}
