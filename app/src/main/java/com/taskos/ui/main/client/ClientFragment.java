package com.taskos.ui.main.client;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentClientBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.client.detail.ClientDetailFragment;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;
import com.taskos.util.AppConstants;


public class ClientFragment extends BaseFragment<FragmentClientBinding, ClientViewModel> implements View.OnClickListener {

    private ClientViewModel clientViewModel;
    private int currentPos = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_client;
    }

    @Override
    public ClientViewModel getViewModel() {
        clientViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(ClientViewModel.class);
        return clientViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().fabAddClient.setOnClickListener(this);
        getViewDataBinding().fabAddJob.setOnClickListener(this);
        getViewDataBinding().fabAddTask.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);

        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });

        doAddTextWatcher();
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            getViewDataBinding().etSearch.setHint("Search All Open Leads");
            getViewDataBinding().cvAllClient.setVisibility(View.GONE);
            doGetClientData(AppConstants.kStatusArray[0], 0, search);
        } else if (currentPos == 1) {
            getViewDataBinding().etSearch.setHint("Search My Leads");
            getViewDataBinding().cvAllClient.setVisibility(View.GONE);
            doGetClientData(AppConstants.kStatusArray[0], 1, search);
        } else {
            getViewDataBinding().etSearch.setHint("Search All Clients");
            getViewDataBinding().cvAllClient.setVisibility(View.VISIBLE);
            doGetClientData(AppConstants.kStatusArray[7], 2, search);
        }
    }

    private void doAddTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    if (editable.toString().length() > 2)
                        filterData(editable.toString());
                } else {
                    filterData("");
                }
            }
        });
    }

    private void doGetClientData(String value, int currentPos, String search) {
        clientViewModel.getOpenClients(value, currentPos, search).observe(getViewLifecycleOwner(), customerMap -> {
                    ClientAdapter clientAdapter = new ClientAdapter(currentPos == 2 ? ClientAdapter.VIEW_TYPE_CLIENT : ClientAdapter.VIEW_TYPE_COMMON, customerMap, customerKey ->
                            getBaseActivity().addFragment(ClientDetailFragment.newInstance(customerKey), R.id.nav_host_fragment, true));
                    getViewDataBinding().rvOpenLeads.setAdapter(clientAdapter);
                }
        );
    }

    @Override
    public void onClick(View view) {
        getViewDataBinding().menuFloat.close(true);
        switch (view.getId()) {
            case R.id.fab_add_client:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.fab_add_job:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.fab_add_task:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.tv_search:
                getViewDataBinding().llSearch.setVisibility(getViewDataBinding().llSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }
}