package com.taskos.ui.main.tasks;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppConstants;
import com.taskos.util.AppLogger;

import java.util.HashMap;

public class TasksViewModel extends BaseViewModel<TasksNavigator> {

    private FirebaseFirestore mFirestore;
    private HashMap<String, ToDo> toDoHashMap = new HashMap<>();
    private MutableLiveData<HashMap<String, ToDo>> mutableLiveData = new MutableLiveData<>();
    private static final String TAG = "TasksViewModel";

    public TasksViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public MutableLiveData<HashMap<String, ToDo>> getOpenTask(String value, int currentPos, String search) {
        toDoHashMap.clear();
        Query query;
        if (currentPos == 0) {
            if (search.isEmpty()) {
                query = mFirestore.collection("ToDo")
                        .orderBy("dateEntered", Query.Direction.DESCENDING);
            } else {
                query = mFirestore.collection("ToDo").whereEqualTo("task", search)
                        .orderBy("dateEntered", Query.Direction.DESCENDING);

            }
        } else if (currentPos == 1) {
            if (search.isEmpty()) {
                query = mFirestore.collection("ToDo").whereEqualTo("owner", AppConstants.ownerId).limit(5);
            } else {
                query = mFirestore.collection("ToDo").whereEqualTo("task", search);
                       /*
                        .whereEqualTo("owner", AppConstants.ownerId).limit(5);*/
            }
        } else {
            if (search.isEmpty()) {
                query = mFirestore.collection("ToDo").orderBy("dateEntered", Query.Direction.DESCENDING);
                      /*.whereEqualTo("completionStatus", value)
                       */
            } else {
                query = mFirestore.collection("ToDo").whereEqualTo("task", search)
                        .orderBy("dateEntered", Query.Direction.DESCENDING);
                       /* .whereEqualTo("completionStatus", value)
                       */
            }
        }

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }
            if (queryDocumentSnapshots != null) {
                toDoHashMap.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    AppLogger.e("Test", "" + doc.getId());
                    ToDo toDo = doc.toObject(ToDo.class);
                    toDoHashMap.put(doc.getId(), toDo);
                    mutableLiveData.setValue(toDoHashMap);
                }
            }
        });
        return mutableLiveData;
    }
}