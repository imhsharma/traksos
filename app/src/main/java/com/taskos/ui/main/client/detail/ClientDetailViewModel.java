package com.taskos.ui.main.client.detail;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.ui.base.BaseViewModel;

public class ClientDetailViewModel extends BaseViewModel<ClientDetailNavigator> {
    private static final String TAG = "ClientDetailViewModel";
    private FirebaseFirestore mFirestore;
    private MutableLiveData<Customer> mutableLiveData = new MutableLiveData<>();


    public ClientDetailViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public MutableLiveData<Customer> getClientDetail(String customerKey) {
        DocumentReference query = mFirestore.collection("Customers").document(customerKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                Customer customer = documentSnapshot.toObject(Customer.class);
                mutableLiveData.setValue(customer);
            }
        });

        return mutableLiveData;
    }

    public void doSaveAppointmentInfo(String customerKey, Customer customer) {
        mFirestore.collection("Customers").document(customerKey).set(customer);
    }
}