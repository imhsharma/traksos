package com.taskos.ui.main.employee;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.other.KeyValue;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class EmployeeAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private ItemClickCallback callback;
    private List<KeyValue> keyValueList;
    private int lastPos = 0;

    public EmployeeAdapter(List<KeyValue> keyValueList, ItemClickCallback callback) {
        this.keyValueList = keyValueList;
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employee, parent, false));
    }

    @Override
    public int getItemCount() {
        if (keyValueList != null && keyValueList.size() > 0) {
            return keyValueList.size();
        } else {
            return 0;
        }
    }


    public class ViewHolder extends BaseViewHolder implements View.OnClickListener {
        private TextView tv_name;
        private RadioButton radio_button;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            radio_button = itemView.findViewById(R.id.radio_button);

            radio_button.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        public void onBind(int position) {
            KeyValue tmpBean = keyValueList.get(position);
            tv_name.setText(tmpBean.getValue());

            radio_button.setChecked(tmpBean.isSelected);
        }

        @Override
        public void onClick(View view) {
            int tempPos = getAdapterPosition();
            if (tempPos != -1 && callback != null) {
                keyValueList.get(lastPos).isSelected = false;
                notifyItemChanged(lastPos);

                callback.onItemClick(getAdapterPosition());
                keyValueList.get(tempPos).isSelected = true;
                notifyItemChanged(tempPos);

                lastPos = tempPos;
            }
        }
    }

}
