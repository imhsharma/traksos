package com.taskos.ui.main.more;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentMoreBinding;
import com.taskos.ui.base.BaseFragment;

public class MoreFragment extends BaseFragment<FragmentMoreBinding, MoreViewModel> {

    private MoreViewModel moreViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_more;
    }

    @Override
    public MoreViewModel getViewModel() {
        moreViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(MoreViewModel.class);
        return moreViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView textView = view.findViewById(R.id.text_dashboard);
        moreViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
    }


}