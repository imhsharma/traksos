package com.taskos.ui.main.client.detail.comment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.UserCommentsCustomer;
import com.taskos.databinding.ActivityCommentBinding;
import com.taskos.ui.base.BaseActivity;

import java.util.Date;
import java.util.UUID;

public class CommentActivity extends BaseActivity<ActivityCommentBinding, CommentViewModel> implements CommentNavigator, View.OnClickListener {

    private CommentViewModel commentViewModel;
    private Customer customer;

    public static Intent newIntent(Context context) {
        return new Intent(context, CommentActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_comment;
    }

    @Override
    public CommentViewModel getViewModel() {
        commentViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(CommentViewModel.class);
        return commentViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentViewModel.setNavigator(this);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.back);
        tvTitle.setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);
        getViewDataBinding().tvSaveComment.setOnClickListener(this);

        commentViewModel.getClientDetail().observe(this, customer ->
        {
            this.customer = customer;
        });

        commentViewModel.getComments().observe(this, commentList -> {
            CommentAdapter commentAdapter = new CommentAdapter(commentList);
            getViewDataBinding().rvComments.setAdapter(commentAdapter);
            commentAdapter.notifyDataSetChanged();
        });
    }


    @Override
    public void clearInput() {
        getViewDataBinding().etComment.setText("");
        getViewDataBinding().tvSaveComment.setEnabled(true);
    }

    @Override
    public void enableInput() {
        getViewDataBinding().tvSaveComment.setEnabled(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.tv_save_comment:
                if (getViewDataBinding().etComment.getText().toString().isEmpty()) {
                    showToast(getString(R.string.alert_comment_empty));
                } else {
                    if (customer != null) {
                        getViewDataBinding().tvSaveComment.setEnabled(false);
                        UserCommentsCustomer userCommentsCustomer = new UserCommentsCustomer();
                        userCommentsCustomer.setUuid(UUID.randomUUID().toString());
                        userCommentsCustomer.setUserFirstName("Test");
                        userCommentsCustomer.setUserLastName("User");
                        userCommentsCustomer.setUserUUID("");
                        userCommentsCustomer.setUsername("Test");
                        userCommentsCustomer.setCustomerUuid(customer.getUuid());
                        userCommentsCustomer.setComment(getViewDataBinding().etComment.getText().toString());
                        userCommentsCustomer.setCommentPostedTime(new Date());
                        userCommentsCustomer.setCustomerStatus(customer.getCustomerStatus());
                        userCommentsCustomer.setImageLink("");
                        userCommentsCustomer.setOwner(customer.getOwner());
                        commentViewModel.sendComment(userCommentsCustomer);
                    } else showToast(getString(R.string.something_went_wrong));
                }
                break;

        }
    }
}
