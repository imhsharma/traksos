package com.taskos.ui.main.openjobs;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskos.R;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.Job;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.ui.base.ItemClickCallback;

import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class OpenJobAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static int VIEW_TYPE_COMMON = 1;
    public static int VIEW_TYPE_JOB = 2;
    private ItemClickCallback callback;
    private List<Job> openJobList;
    private int viewType;

    public OpenJobAdapter(int viewType, List<Customer> openJobList, ItemClickCallback callback) {
      /*  this.openJobList = customerList;
        mKeys = new ArrayList<>(customerList.keySet());*/
        setViewType(viewType);
        this.callback = callback;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_COMMON) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ready_jobs, parent, false));
        } else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_go_jobs, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_TYPE_COMMON) {
            return VIEW_TYPE_COMMON;
        } else {
            return VIEW_TYPE_JOB;
        }
    }

    @Override
    public int getItemCount() {
        if (openJobList != null && openJobList.size() > 0) {
            return openJobList.size();
        } else {
            return 3;
        }
    }


    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tv_city, tv_sold_on_label, tv_start_job_label;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_city = itemView.findViewById(R.id.tv_city);
            tv_sold_on_label = itemView.findViewById(R.id.tv_sold_on_label);
            tv_start_job_label = itemView.findViewById(R.id.tv_start_job_label);

            itemView.setOnClickListener(view -> {
                if (callback != null && getAdapterPosition() != -1) {
                    callback.onItemClick(getAdapterPosition());
                }
            });
        }

        public void onBind(int position) {
            //Job tmpBean = getItem(position);

            if (viewType == VIEW_TYPE_COMMON) {
                tv_name.setPaintFlags(tv_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tv_sold_on_label.setPaintFlags(tv_sold_on_label.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tv_start_job_label.setPaintFlags(tv_start_job_label.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                 /*   tv_name.setText(tmpBean.getName());
                tv_salesperson.setText(tmpBean.getSalesperson());
                tv_appoint_time.setText(CalenderUtils.formatDate(tmpBean.getAppointmentTime(), CalenderUtils.TIMESTAMP_FORMAT));
                tv_city.setText(tmpBean.getCity());
                tv_status.setText(tmpBean.getCustomerStatus());*/
            } else {

            }

        }
    }
}
