package com.taskos.ui.main.openjobs.addjob;

import com.taskos.data.DataManager;
import com.taskos.ui.base.BaseNavigator;
import com.taskos.ui.base.BaseViewModel;

public class AddJobViewModel extends BaseViewModel<BaseNavigator> {
    public AddJobViewModel(DataManager dataManager) {
        super(dataManager);
    }
}