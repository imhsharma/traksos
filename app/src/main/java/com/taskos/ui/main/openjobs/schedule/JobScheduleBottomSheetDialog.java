package com.taskos.ui.main.openjobs.schedule;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.DialogScheduleJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.util.CalenderUtils;

import java.util.Date;


public class JobScheduleBottomSheetDialog extends BaseBottomSheetDialog<DialogScheduleJobBinding, JobScheduleViewModel> implements View.OnClickListener {

    private static final String TAG = "JobScheduleBottomSheetD";
    private JobScheduleViewModel jobScheduleViewModel;
    private Date startDate = new Date(), endDate = new Date();

    public static JobScheduleBottomSheetDialog newInstance() {

        Bundle args = new Bundle();

        JobScheduleBottomSheetDialog fragment = new JobScheduleBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_schedule_job;
    }

    @Override
    public JobScheduleViewModel getViewModel() {
        jobScheduleViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(JobScheduleViewModel.class);
        return jobScheduleViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvStartDate.setOnClickListener(this);
        getViewDataBinding().tvEndDate.setOnClickListener(this);
        getViewDataBinding().rlAssignEmployee.setOnClickListener(this);
        getViewDataBinding().tvScheduleJob.setOnClickListener(this);
        getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(startDate, CalenderUtils.TIMESTAMP_FORMAT2));
        getViewDataBinding().tvEndDate.setText(CalenderUtils.formatDate(endDate, CalenderUtils.TIMESTAMP_FORMAT2));
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start_date:
                getViewDataBinding().viewStartTime.setVisibility(View.VISIBLE);
                getViewDataBinding().startTimePicker.setVisibility(View.VISIBLE);

                getViewDataBinding().startTimePicker.addOnDateChangedListener((displayed, date) -> {
                    startDate = date;
                    getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT2));
                });
                break;

            case R.id.tv_end_date:
                getViewDataBinding().viewEndTime.setVisibility(View.VISIBLE);
                getViewDataBinding().endTimePicker.setVisibility(View.VISIBLE);

                getViewDataBinding().endTimePicker.addOnDateChangedListener((displayed, date) -> {
                    endDate = date;
                    getViewDataBinding().tvEndDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT2));
                });
                break;

            case R.id.rl_assign_employee:
                EmployeesBottomSheetDialog.newInstance(keyValue -> {

                }).show(getChildFragmentManager());
                break;

            case R.id.tv_schedule_job:
                dismissDialog(TAG);
                break;
        }
    }


}
