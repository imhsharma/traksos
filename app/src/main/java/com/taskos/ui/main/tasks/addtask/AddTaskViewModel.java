package com.taskos.ui.main.tasks.addtask;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewModel;

public class AddTaskViewModel extends BaseViewModel<AddTaskNavigator> {

    private FirebaseFirestore mFirestore;

    public AddTaskViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public void doAddTask(ToDo toDo) {
        // Get a reference to the Customers collection
        CollectionReference customerRef = mFirestore.collection("ToDo");

        customerRef.add(toDo).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().openDashboard();
            }
        });
    }
}