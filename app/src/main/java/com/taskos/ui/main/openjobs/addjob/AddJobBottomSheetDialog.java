package com.taskos.ui.main.openjobs.addjob;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.DialogAddJobBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.util.CalenderUtils;

import java.util.Date;


public class AddJobBottomSheetDialog extends BaseBottomSheetDialog<DialogAddJobBinding, AddJobViewModel> implements View.OnClickListener {

    private static final String TAG = "AddClientBottomSheetDia";
    private AddJobViewModel addJobViewModel;
    private Date startDate;

    public static AddJobBottomSheetDialog newInstance() {

        Bundle args = new Bundle();

        AddJobBottomSheetDialog fragment = new AddJobBottomSheetDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_job;
    }

    @Override
    public AddJobViewModel getViewModel() {
        addJobViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddJobViewModel.class);
        return addJobViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().tvStartDate.setOnClickListener(this);
        getViewDataBinding().tvSave.setOnClickListener(this);
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.tv_start_date:
                getViewDataBinding().viewStartTime.setVisibility(View.VISIBLE);
                getViewDataBinding().startTimePicker.setVisibility(View.VISIBLE);

                getViewDataBinding().startTimePicker.addOnDateChangedListener((displayed, date) -> {
                    startDate = date;
                    getViewDataBinding().tvStartDate.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_save:
                break;
        }
    }


}
