package com.taskos.ui.main.tasks.detail.mytaskdetail;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewModel;

public class TasksDetailViewModel extends BaseViewModel<TasksDetailNavigator> {
    private static final String TAG = "ClientDetailViewModel";
    private FirebaseFirestore mFirestore;
    private MutableLiveData<ToDo> mutableLiveData = new MutableLiveData<>();


    public TasksDetailViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public MutableLiveData<ToDo> getTaskDetail(String taskKey) {
        DocumentReference query = mFirestore.collection("ToDo").document(taskKey);

        query.addSnapshotListener((documentSnapshot, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (documentSnapshot != null) {
                ToDo toDo = documentSnapshot.toObject(ToDo.class);
                mutableLiveData.setValue(toDo);
            }
        });

        return mutableLiveData;
    }

    public void doSaveAppointmentInfo(String taskKey, ToDo toDo) {
        mFirestore.collection("ToDo").document(taskKey).set(toDo);
    }
}