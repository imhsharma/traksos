package com.taskos.ui.main.tasks.addtask;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public interface AddTaskNavigator {
    void openDashboard();
}
