package com.taskos.ui.main.client.detail.comment;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.taskos.data.DataManager;
import com.taskos.data.model.api.Customer;
import com.taskos.data.model.api.UserCommentsCustomer;
import com.taskos.ui.base.BaseViewModel;
import com.taskos.util.AppLogger;

import java.util.ArrayList;
import java.util.List;

public class CommentViewModel extends BaseViewModel<CommentNavigator> {

    private static final String TAG = "CommentViewModel";
    private FirebaseFirestore mFirestore;
    private MutableLiveData<Customer> customerMutableLiveData = new MutableLiveData<>();
    private ArrayList<UserCommentsCustomer> userCommentsArrayList = new ArrayList<>();
    private MutableLiveData<List<UserCommentsCustomer>> commentsListMutableLiveData = new MutableLiveData<>();


    public CommentViewModel(DataManager dataManager) {
        super(dataManager);
        mFirestore = FirebaseFirestore.getInstance();
    }

    public MutableLiveData<Customer> getClientDetail() {
        Query query = mFirestore.collection("Customers");

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    AppLogger.e("Test", "" + doc.getId());
                    Customer customer = doc.toObject(Customer.class);
                    customerMutableLiveData.setValue(customer);
                }
            }
        });

        return customerMutableLiveData;
    }

    public MutableLiveData<List<UserCommentsCustomer>> getComments() {
        Query query = mFirestore.collection("Comments").orderBy("commentPostedTime", Query.Direction.DESCENDING);

        query.addSnapshotListener((queryDocumentSnapshots, e) -> {
            if (e != null) {
                Log.e(TAG, "onEvent: Listen failed.", e);
                return;
            }

            if (queryDocumentSnapshots != null) {
                userCommentsArrayList.clear();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    UserCommentsCustomer userCommentsCustomer = doc.toObject(UserCommentsCustomer.class);
                    userCommentsArrayList.add(userCommentsCustomer);
                    commentsListMutableLiveData.setValue(userCommentsArrayList);
                }
            }
        });

        return commentsListMutableLiveData;
    }

    public void sendComment(UserCommentsCustomer userCommentsCustomer) {
        // Get a reference to the Customers collection
        CollectionReference customerRef = mFirestore.collection("Comments");

        customerRef.add(userCommentsCustomer).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getNavigator().clearInput();
            } else {
                getNavigator().enableInput();
            }
        });
    }
}
