package com.taskos.ui.main.tasks;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.taskos.R;
import com.taskos.data.model.api.ToDo;
import com.taskos.ui.base.BaseViewHolder;
import com.taskos.util.CalenderUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class TaskAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    static int VIEW_TYPE_COMMON = 1;
    static int VIEW_ALL_OPEN_TASK = 2;
    private ItemClickListener callback;
    private HashMap<String, ToDo> todoList;
    private List<String> mKeys;
    private int viewType;

    TaskAdapter(int viewType, HashMap<String, ToDo> todoList, ItemClickListener callback) {
        this.viewType = viewType;
        this.todoList = todoList;
        mKeys = new ArrayList<>(todoList.keySet());
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ALL_OPEN_TASK) {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_open_task, parent, false));
        } else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_task, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == VIEW_ALL_OPEN_TASK) {
            return VIEW_ALL_OPEN_TASK;
        } else {
            return VIEW_TYPE_COMMON;
        }
    }

    @Override
    public int getItemCount() {
        if (todoList != null && todoList.size() > 0) {
            return todoList.size();
        } else {
            return 0;
        }
    }

    private ToDo getItem(int position) {
        return todoList.get(mKeys.get(position));
    }

    public interface ItemClickListener {
        void onItemClick(String userKey);
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView tv_name, tvTaskName, tv_status, tv_appoint_time;
        private FrameLayout frameBackGroundLayout;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tvTaskName = itemView.findViewById(R.id.tvTaskName);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_appoint_time = itemView.findViewById(R.id.tv_appoint_time);
            frameBackGroundLayout = itemView.findViewById(R.id.frameBackGroundLayout);

            itemView.setOnClickListener(view -> {
                if (callback != null && getAdapterPosition() != -1) {
                    callback.onItemClick(mKeys.get(getAdapterPosition()));
                }
            });
        }

        public void onBind(int position) {
            ToDo tmpBean = getItem(position);

            if (viewType == VIEW_ALL_OPEN_TASK) {
                tv_name.setText(tmpBean.getOwner());
                tvTaskName.setText(tmpBean.getTask());
                tv_status.setText(tmpBean.getCompletionStatus());
                if (tmpBean.getCompletionStatus().equals("Open"))
                    frameBackGroundLayout.setBackgroundResource(R.drawable.bg_green_drawable);
                else
                    frameBackGroundLayout.setBackgroundResource(R.drawable.bg_red_drawable);
            } else {
                tv_name.setPaintFlags(tv_name.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tv_name.setText(tmpBean.getOwner());
                tvTaskName.setText(tmpBean.getTask());
                tv_appoint_time.setText(CalenderUtils.formatDate(tmpBean.getDateEntered(), CalenderUtils.TIMESTAMP_FORMAT));
                tv_status.setText(tmpBean.getCompletionStatus());
                if (tmpBean.getCompletionStatus().equals("Open"))
                    frameBackGroundLayout.setBackgroundResource(R.drawable.bg_green_drawable);
                else
                    frameBackGroundLayout.setBackgroundResource(R.drawable.bg_red_drawable);
            }
        }
    }
}
