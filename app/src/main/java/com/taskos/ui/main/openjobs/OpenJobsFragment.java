package com.taskos.ui.main.openjobs;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.FragmentOpenJobsBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.client.addclient.AddClientBottomSheetDialog;
import com.taskos.ui.main.openjobs.addjob.AddJobBottomSheetDialog;
import com.taskos.ui.main.openjobs.detail.JobDetailFragment;
import com.taskos.ui.main.tasks.addtask.AddTaskBottomSheetDialog;
import com.taskos.util.AppConstants;

import java.util.ArrayList;


public class OpenJobsFragment extends BaseFragment<FragmentOpenJobsBinding, OpenJobsViewModel> implements View.OnClickListener {

    private OpenJobsViewModel openJobsViewModel;
    private int currentPos = 0;
    private OpenJobAdapter openJobAdapter;

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_open_jobs;
    }

    @Override
    public OpenJobsViewModel getViewModel() {
        openJobsViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(OpenJobsViewModel.class);
        return openJobsViewModel;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().fabAddClient.setOnClickListener(this);
        getViewDataBinding().fabAddJob.setOnClickListener(this);
        getViewDataBinding().fabAddTask.setOnClickListener(this);
        getViewDataBinding().tvSearch.setOnClickListener(this);

        filterData("");
        getViewDataBinding().segmentedButtonGroup.setOnClickedButtonListener(position -> {
            currentPos = position;
            filterData("");
        });

        doAddTextWatcher();
    }

    private void filterData(String search) {
        if (currentPos == 0) {
            getViewDataBinding().etSearch.setHint("Search Ready Jobs");
            doGetClientData(AppConstants.kStatusArray[0], 0, search);
        } else if (currentPos == 1) {
            getViewDataBinding().etSearch.setHint("Search Set Jobs");
            doGetClientData(AppConstants.kStatusArray[0], 1, search);
        } else {
            getViewDataBinding().etSearch.setHint("Search Go Jobs");
            doGetClientData(AppConstants.kStatusArray[7], 2, search);
        }
    }

    private void doAddTextWatcher() {
        getViewDataBinding().etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    if (editable.toString().length() > 2)
                        filterData(editable.toString());
                } else {
                    filterData("");
                }
            }
        });
    }

    private void doGetClientData(String value, int currentPos, String search) {
       /* openJobsViewModel.getOpenClients(value, currentPos, search).observe(getViewLifecycleOwner(), customerMap -> {
                    ClientAdapter clientAdapter = new ClientAdapter(currentPos == 2 ? ClientAdapter.VIEW_TYPE_CLIENT : ClientAdapter.VIEW_TYPE_COMMON, customerMap, customerKey ->
                            getBaseActivity().addFragment(ClientDetailFragment.newInstance(customerKey), R.id.nav_host_fragment, true));
                    getViewDataBinding().rvOpenLeads.setAdapter(clientAdapter);
                }
        );*/

        if (openJobAdapter == null) {
            openJobAdapter = new OpenJobAdapter(currentPos == 2 ? OpenJobAdapter.VIEW_TYPE_JOB : OpenJobAdapter.VIEW_TYPE_COMMON,
                    new ArrayList<>(), pos -> getBaseActivity().addFragment(JobDetailFragment.newInstance(""), R.id.nav_host_fragment, true));
            getViewDataBinding().recyclerViewJobs.setAdapter(openJobAdapter);
        } else {
            openJobAdapter.setViewType(currentPos == 2 ? OpenJobAdapter.VIEW_TYPE_JOB : OpenJobAdapter.VIEW_TYPE_COMMON);
            openJobAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View view) {
        getViewDataBinding().menuFloat.close(true);
        switch (view.getId()) {
            case R.id.fab_add_client:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddClientBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.fab_add_job:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddJobBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.fab_add_task:
                getViewDataBinding().llSearch.setVisibility(View.GONE);
                AddTaskBottomSheetDialog.newInstance().show(getChildFragmentManager());
                break;

            case R.id.tv_search:
                getViewDataBinding().llSearch.setVisibility(getViewDataBinding().llSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                break;
        }
    }
}