package com.taskos.ui.main.client.detail.addevent;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.databinding.DialogAddEventBinding;
import com.taskos.ui.base.BaseBottomSheetDialog;
import com.taskos.ui.main.client.ClientNavigator;
import com.taskos.ui.main.client.detail.eventrepeat.EventCommonBottomSheetDialog;
import com.taskos.util.CalenderUtils;

import java.util.Date;


public class AddEventBottomSheetDialog extends BaseBottomSheetDialog<DialogAddEventBinding, AddEventViewModel> implements View.OnClickListener, ClientNavigator {

    private static final String TAG = "AddEventBottomSheetDial";
    private AddEventViewModel addClientViewModel;
    private Date startTime, endTime;
    private String name = "", repeatValue = "";

    public static AddEventBottomSheetDialog newInstance(String name) {

        Bundle args = new Bundle();

        AddEventBottomSheetDialog fragment = new AddEventBottomSheetDialog();
        args.putString("Name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return com.taskos.BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_add_event;
    }

    @Override
    public AddEventViewModel getViewModel() {
        addClientViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(AddEventViewModel.class);
        return addClientViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addClientViewModel.setNavigator(this);
        if (getArguments() != null) {
            name = getArguments().getString("Name");
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewDataBinding().tvName.setText(name.concat(" Appointment: "));
        getViewDataBinding().frameCancel.setOnClickListener(this);
        getViewDataBinding().tvCancel.setOnClickListener(this);
        getViewDataBinding().llStartTime.setOnClickListener(this);
        getViewDataBinding().llEndTime.setOnClickListener(this);
        getViewDataBinding().tvAdd.setOnClickListener(this);
        getViewDataBinding().rlRepeat.setOnClickListener(this);

        startTime = new Date();
        endTime = CalenderUtils.addHoursToJavaUtilDate(startTime, 2);
        getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(startTime, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
        getViewDataBinding().tvEndTime.setText(CalenderUtils.formatDate(endTime, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                dismissDialog(TAG);
                break;

            case R.id.ll_start_time:
                getViewDataBinding().view.setVisibility(View.VISIBLE);
                getViewDataBinding().dateTimePicker.setVisibility(View.VISIBLE);
                getViewDataBinding().dateTimePicker.addOnDateChangedListener((displayed, date) -> {
                    startTime = date;
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(date, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
                });
                break;

            case R.id.ll_end_time:
                getViewDataBinding().viewEnd.setVisibility(View.VISIBLE);
                getViewDataBinding().dateTimePickerEnd.setVisibility(View.VISIBLE);
                getViewDataBinding().dateTimePickerEnd.setDefaultDate(CalenderUtils.addHoursToJavaUtilDate(startTime, 2));
                getViewDataBinding().dateTimePickerEnd.setMinDate(CalenderUtils.addHoursToJavaUtilDate(startTime, 2));
                getViewDataBinding().dateTimePickerEnd.addOnDateChangedListener((displayed, date) -> {
                    endTime = date;
                    getViewDataBinding().tvEndTime.setText(CalenderUtils.formatDate(date, CalenderUtils.CUSTOM_TIMESTAMP_FORMAT_DASH));
                });
                break;

            case R.id.frame_cancel:
                getViewDataBinding().etAddress.setText("");
                break;

            case R.id.rl_repeat:
                EventCommonBottomSheetDialog.newInstance("Repeat", keyValue -> {
                    repeatValue = keyValue.getKey();
                    getViewDataBinding().tvRepeat.setText(keyValue.getValue());
                }).show(getChildFragmentManager());
                break;

            case R.id.tv_add:
                if (verifyInputs()) {
                    addEventToCalendar();
                }
                break;
        }
    }

    private void addEventToCalendar() {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");

        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, getViewDataBinding().switchAllday.isChecked());
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime.getTime());
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTime());

        if (!repeatValue.isEmpty())
            intent.putExtra(CalendarContract.Events.RRULE, repeatValue);  //YEARLY, MONTHLY, WEEKLY, DAILY

        intent.putExtra(CalendarContract.Events.EVENT_COLOR, R.color.light_pink);
        intent.putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        intent.putExtra(CalendarContract.Events.TITLE, name.concat(" Appointment: ").concat(getViewDataBinding().etAppointName.getText().toString()));
        intent.putExtra(CalendarContract.Events.DESCRIPTION, "");
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, getViewDataBinding().etAddress.getText().toString());

        startActivity(intent);
        dismissDialog(TAG);
    }

    private boolean verifyInputs() {
        if (getViewDataBinding().etAppointName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_name_empty));
            return false;
        } else return true;
    }
}
