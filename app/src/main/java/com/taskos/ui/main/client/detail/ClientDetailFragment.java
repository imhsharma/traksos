package com.taskos.ui.main.client.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.taskos.BR;
import com.taskos.R;
import com.taskos.ViewModelProviderFactory;
import com.taskos.data.model.api.Customer;
import com.taskos.databinding.FragmentClientDetailBinding;
import com.taskos.ui.base.BaseFragment;
import com.taskos.ui.main.client.detail.addevent.AddEventBottomSheetDialog;
import com.taskos.ui.main.client.detail.comment.CommentActivity;
import com.taskos.ui.main.client.detail.status_sold.SoldBottomSheetDialog;
import com.taskos.ui.main.employee.EmployeesBottomSheetDialog;
import com.taskos.util.CalenderUtils;
import com.taskos.util.PermissionUtils;

import java.util.Date;

public class ClientDetailFragment extends BaseFragment<FragmentClientDetailBinding, ClientDetailViewModel> implements ClientDetailNavigator, View.OnClickListener {

    private ClientDetailViewModel clientDetailViewModel;
    private String phone = "", email = "";
    private String customerKey = "";
    private Date appointmentTime, appointmentTimeSet;
    private Customer customer;

    public static ClientDetailFragment newInstance(String customerKey) {

        Bundle args = new Bundle();

        ClientDetailFragment fragment = new ClientDetailFragment();
        args.putString("CustomerKey", customerKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_client_detail;
    }

    @Override
    public ClientDetailViewModel getViewModel() {
        clientDetailViewModel = new ViewModelProvider(this, ViewModelProviderFactory.getInstance()).get(ClientDetailViewModel.class);
        return clientDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clientDetailViewModel.setNavigator(this);
        if (getArguments() != null) {
            customerKey = getArguments().getString("CustomerKey");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.clients);
        tvTitle.setOnClickListener(this);
        view.findViewById(R.id.img_back).setOnClickListener(this);
        getViewDataBinding().tvAddCalendar.setOnClickListener(this);
        getViewDataBinding().tvSold.setOnClickListener(this);
        getViewDataBinding().rlComments.setOnClickListener(this);
        getViewDataBinding().tvCall.setOnClickListener(this);
        getViewDataBinding().tvEmail.setOnClickListener(this);
        getViewDataBinding().tvDirection.setOnClickListener(this);
        getViewDataBinding().tvEditAppointment.setOnClickListener(this);
        getViewDataBinding().tvSaveAppointment.setOnClickListener(this);
        getViewDataBinding().tvAppointTime.setOnClickListener(this);
        getViewDataBinding().tvAppointSet.setOnClickListener(this);
        getViewDataBinding().tvSaveContact.setOnClickListener(this);
        getViewDataBinding().tvEditCustInfo.setOnClickListener(this);
        getViewDataBinding().llSalesPerson.setOnClickListener(this);
        getViewDataBinding().tvEditSalesPerson.setOnClickListener(this);
        getViewDataBinding().tvSaveSalesPerson.setOnClickListener(this);

        clientDetailViewModel.getClientDetail(customerKey).observe(getViewLifecycleOwner(), customer -> {
            this.customer = customer;
            getViewDataBinding().tvSalesPersonName.setText(customer.getSalesperson());
            getViewDataBinding().tvNameMap.setText(customer.getName());
            getViewDataBinding().tvName.setText(customer.getName());
            getViewDataBinding().etName.setText(customer.getName());
            getViewDataBinding().etProductInterest.setText(customer.getProductOfInterest());
            appointmentTime = customer.getAppointmentTime();
            appointmentTimeSet = customer.getAppointmentEntered();
            getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.TIME_FORMAT_AM)));
            getViewDataBinding().tvAppointSet.setText(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.TIME_FORMAT_AM)));
            getViewDataBinding().etLeadSource.setText(customer.getSource());
            getViewDataBinding().etStatus.setText(customer.getCustomerStatus());
            getViewDataBinding().tvStreet.setText(customer.getStreetAddress());
            getViewDataBinding().tvAddress.setText(customer.getCity().concat(", ").concat(customer.getState()).concat(" ").concat(customer.getZipcode()));
            phone = customer.getPhone();
            email = customer.getEmail();
            getViewDataBinding().etPhone.setText(customer.getPhone());
            getViewDataBinding().etEmail.setText(customer.getEmail());
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTitle:
            case R.id.img_back:
                getBaseActivity().onBackPressed();
                break;

            case R.id.tv_add_calendar:
                AddEventBottomSheetDialog.newInstance(customer.getName()).show(getChildFragmentManager());
                break;

            case R.id.tv_sold:
                SoldBottomSheetDialog.newInstance(customerKey).show(getChildFragmentManager());
                break;

            case R.id.rl_comments:
                Intent intent = CommentActivity.newIntent(getBaseActivity());
                startActivity(intent);
                break;

            case R.id.tv_call:
                if (PermissionUtils.requestCallPermission(getBaseActivity())) {
                    intent = new Intent(Intent.ACTION_CALL);

                    intent.setData(Uri.parse("tel:" + phone));
                    startActivity(intent);
                }
                break;

            case R.id.tv_email:
                startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:".concat(email))));
                break;

            case R.id.tv_direction:
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:22.7196, 75.8577?q=22.7196, 75.8577(Indore)"));
                /*Uri gmmIntentUri = Uri.parse("geo: 22.7196, 75.8577");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);*/
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getBaseActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                break;

            case R.id.tv_edit_appointment:
                if (getViewDataBinding().tvEditAppointment.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditAppointment.setText(R.string.cancel);
                    enableDisableAppointmentView(true);
                } else {
                    enableDisableAppointmentView(false);
                    getViewDataBinding().tvEditAppointment.setText(R.string.edit);
                    getViewDataBinding().etName.setText(customer.getName());
                    appointmentTime = customer.getAppointmentTime();
                    appointmentTimeSet = customer.getAppointmentEntered();
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentTime(), CalenderUtils.TIME_FORMAT_AM)));
                    getViewDataBinding().tvAppointSet.setText(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.CUSTOM_TIMESTAMP_FORMAT).concat(" at ").concat(CalenderUtils.formatDate(customer.getAppointmentEntered(), CalenderUtils.TIME_FORMAT_AM)));
                    getViewDataBinding().etLeadSource.setText(customer.getSource());
                    getViewDataBinding().etProductInterest.setText(customer.getProductOfInterest());
                }
                break;

            case R.id.tv_save_appointment:
                if (isNetworkConnected()) {
                    if (verifyInput()) {
                        enableDisableAppointmentView(false);
                        customer.setName(getViewDataBinding().etName.getText().toString());
                        customer.setAppointmentTime(appointmentTime);
                        customer.setAppointmentEntered(appointmentTimeSet);
                        customer.setSource(getViewDataBinding().etLeadSource.getText().toString());
                        customer.setProductOfInterest(getViewDataBinding().etProductInterest.getText().toString());

                        clientDetailViewModel.doSaveAppointmentInfo(customerKey, customer);
                        getViewDataBinding().tvEditAppointment.setText(R.string.edit);
                    }
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_appoint_time:
                getViewDataBinding().viewTime.setVisibility(View.VISIBLE);
                getViewDataBinding().dateAppointTimePicker.setVisibility(View.VISIBLE);
                getViewDataBinding().dateAppointTimePicker.addOnDateChangedListener((displayed, date) -> {
                    appointmentTime = date;
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_appoint_set:
                getViewDataBinding().viewSet.setVisibility(View.VISIBLE);
                getViewDataBinding().dateAppointSetPicker.setVisibility(View.VISIBLE);
                getViewDataBinding().dateAppointSetPicker.addOnDateChangedListener((displayed, date) -> {
                    appointmentTimeSet = date;
                    getViewDataBinding().tvAppointTime.setText(CalenderUtils.formatDate(date, CalenderUtils.TIMESTAMP_FORMAT));
                });
                break;

            case R.id.tv_edit_cust_info:
                if (getViewDataBinding().tvEditCustInfo.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditCustInfo.setText(R.string.cancel);
                    enableDisableContactView(true);
                } else {
                    getViewDataBinding().tvEditCustInfo.setText(R.string.edit);
                    enableDisableContactView(false);
                    getViewDataBinding().etPhone.setText(customer.getPhone());
                    getViewDataBinding().etEmail.setText(customer.getEmail());
                }
                break;

            case R.id.tv_save_contact:
                if (isNetworkConnected()) {
                    enableDisableContactView(false);
                    customer.setPhone(getViewDataBinding().etPhone.getText().toString());
                    customer.setEmail(getViewDataBinding().etEmail.getText().toString());
                    clientDetailViewModel.doSaveAppointmentInfo(customerKey, customer);
                    getViewDataBinding().tvEditCustInfo.setText(R.string.edit);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;

            case R.id.tv_edit_sales_person:
                if (getViewDataBinding().tvEditSalesPerson.getText().toString().equals("Edit")) {
                    getViewDataBinding().tvEditSalesPerson.setText(R.string.cancel);
                    getViewDataBinding().tvAddCalendar.setVisibility(View.GONE);
                    getViewDataBinding().tvSaveSalesPerson.setVisibility(View.VISIBLE);
                } else {
                    getViewDataBinding().tvEditSalesPerson.setText(R.string.edit);
                    getViewDataBinding().tvAddCalendar.setVisibility(View.VISIBLE);
                    getViewDataBinding().tvSaveSalesPerson.setVisibility(View.GONE);
                    getViewDataBinding().tvSalesPersonName.setText(customer.getSalesperson());
                }
                break;

            case R.id.ll_sales_person:
                if (getViewDataBinding().tvEditSalesPerson.getText().toString().equals("Cancel")) {
                    EmployeesBottomSheetDialog.newInstance(keyValue -> {
                        getViewDataBinding().tvSalesPersonName.setText(keyValue.getValue());
                    }).show(getChildFragmentManager());
                }
                break;

            case R.id.tv_save_sales_person:
                if (isNetworkConnected()) {
                    customer.setSalesperson(getViewDataBinding().tvSalesPersonName.getText().toString());
                    clientDetailViewModel.doSaveAppointmentInfo(customerKey, customer);

                    getViewDataBinding().tvEditAppointment.setText(R.string.edit);
                    getViewDataBinding().tvAddCalendar.setVisibility(View.VISIBLE);
                    getViewDataBinding().tvSaveSalesPerson.setVisibility(View.GONE);
                } else {
                    showToast(getString(R.string.noInternet));
                }
                break;
        }
    }

    private void enableDisableContactView(boolean isEnable) {
        getViewDataBinding().tvCall.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().viewCall.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().tvEmail.setVisibility(isEnable ? View.GONE : View.VISIBLE);
        getViewDataBinding().tvSaveContact.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().etPhone.setEnabled(isEnable);
        getViewDataBinding().etEmail.setEnabled(isEnable);
    }

    private boolean verifyInput() {
        if (getViewDataBinding().etName.getText().toString().isEmpty()) {
            showToast(getString(R.string.alert_cust_name_empty));
            return false;
        } else return true;
    }

    private void enableDisableAppointmentView(boolean isEnable) {
        if (!isEnable) {
            getViewDataBinding().viewTime.setVisibility(View.GONE);
            getViewDataBinding().dateAppointTimePicker.setVisibility(View.GONE);

            getViewDataBinding().viewSet.setVisibility(View.GONE);
            getViewDataBinding().dateAppointSetPicker.setVisibility(View.GONE);
        }
        getViewDataBinding().viewSaveAppointment.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().tvSaveAppointment.setVisibility(isEnable ? View.VISIBLE : View.GONE);
        getViewDataBinding().tvSaveAppointment.setEnabled(isEnable);
        getViewDataBinding().etName.setEnabled(isEnable);
        getViewDataBinding().etProductInterest.setEnabled(isEnable);
        getViewDataBinding().etLeadSource.setEnabled(isEnable);
    }

}