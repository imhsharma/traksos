package com.taskos;

import android.app.Application;

import com.taskos.data.AppDataManager;


/**
 * Created by Hemant Sharma on 23-02-20.
 */
public class TaskOS extends Application {
    private static TaskOS instance;
    private AppDataManager appInstance;

    public static synchronized TaskOS getInstance() {
        if (instance != null) {
            return instance;
        }
        return new TaskOS();
    }

    public AppDataManager getDataManager() {
        return appInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appInstance = AppDataManager.getInstance(this);
    }

}
